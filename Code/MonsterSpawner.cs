using Godot;
using System;
using System.Collections.Generic;

public class MonsterSpawner : Node2D
{
	private int currentWidth = 0;
	private List<SpawnItem> items;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		items = new List<SpawnItem>();
	}

	public void Add(SpawnItem item)
	{
		Vector2 position = new Vector2(currentWidth, Position.y - item.Size.y);
		item.Position = position;

		currentWidth += (int)item.Size.x;

		items.Add(item);
	}

	public void Remove(int index)
	{
		items[index].Remove();
		items.RemoveAt(index);
		UpdateLineUp();
	}

	public void HurtAnimation(int index, int currentHealth)
	{
		items[index].Hurt(currentHealth);
	}

	public void AttackAnimation(int index)
	{
		items[index].Attack();
	}

	private void UpdateLineUp()
	{
		currentWidth = 0;
		foreach(SpawnItem item in items)
		{
			if(item.Position.x != currentWidth)
			{
				Tween tween = GetNode("Tween") as Tween;
				tween.InterpolateProperty(item, "position", item.Position, new Vector2(currentWidth, item.Position.y), 0.1f, Tween.TransitionType.Linear, Tween.EaseType.InOut);
				tween.Start();
			}
			currentWidth += (int)item.Size.x;
		}
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
