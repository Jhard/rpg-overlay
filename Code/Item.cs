using Godot;
using System;

public class Item : SpawnItem 
{
	private static PackedScene ItemScene = null;
	private ItemStats stats;
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	// Called when the node enters the scene tree for the first time.
	/*public override void _Ready()
	{}*/

	public void SetStats(ItemStats _stats)
	{
		stats = _stats;
		var itemSprite = GetNode("Body/ItemSprite") as Sprite;
		itemSprite.Texture = stats.ItemImage;
		Size = new Vector2(itemSprite.Texture.GetWidth(), itemSprite.Texture.GetHeight());
	}

	public static Item CreateItem(String name, Node parent)
	{
		ItemScene = GD.Load<PackedScene>("res://Scenes/Item.tscn");
		var stats = GD.Load<ItemStats>("res://Items/" + name + ".tres");
		var item = ItemScene.Instance() as Item;
		parent.AddChild(item);
		item.SetStats(stats);

		return item;
	}
//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
