using Godot;
using System;

public class ItemStats : Resource
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    [Export]
    public Texture ItemImage {get; set;}
}
