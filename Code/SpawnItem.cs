using Godot;

public abstract class SpawnItem : Node2D
{
	private static readonly string IntroAnimation = "MonsterIntro";
	private static readonly string ExitAnimation = "MonsterExit";
	private static readonly string HurtAnimation = "MonsterHurt";
	private static readonly string AttackAnimation = "MonsterAttack";
	private static readonly Vector2 GScale = new Vector2(1f, 1f);

	private AnimationPlayer monsterAnimation = null;
	private Vector2 size;

	public Vector2 Size
	{
		get
		{
			return size * GScale;
		}
		protected set
		{
			size = value;
		}
	}

	public override void _Ready()
	{
		Scale = GScale;
		monsterAnimation = GetNode("Body/ItemAnimationPlayer") as AnimationPlayer;
		monsterAnimation.Play(IntroAnimation);
		monsterAnimation.Connect("animation_finished", this, nameof(OnAnimationFinished));
	}

	public void Remove()
	{
		monsterAnimation.Play(ExitAnimation);
	}

	public virtual void Hurt(int currentHealth)
	{
		monsterAnimation.Play(HurtAnimation);
	}

	public void Attack()
	{
		monsterAnimation.Play(AttackAnimation);
	}

	public void OnAnimationFinished(string animName)
	{
		if(animName == ExitAnimation)
		{
			QueueFree();
		}
	}
}
