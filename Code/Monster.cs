using Godot;
using System;
using System.IO;

public class Monster : SpawnItem
{
	private static readonly string IntroAnimation = "MonsterIntro";
	private static readonly string ExitAnimation = "MonsterExit";
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	private static PackedScene monsterScene = null;
	private MonsterStats stats;
	private Control healthBar = null;

	private int TotalHealth {get; set;}

	// Called when the node enters the scene tree for the first time.
	/*public override void _Ready()
	{

	}*/
	
	public void SetStats(MonsterStats _stats, int totalHealth, int level)
	{
		stats = _stats;
		var monsterSprite = GetNode("Body/MonsterFrames") as AnimatedSprite;
		monsterSprite.Frames = stats.MonsterFrames;

		Texture texture = monsterSprite.Frames.GetFrame("Idle", 0);

		var status = GetNode("Body/Status") as Control;

		TotalHealth = totalHealth;
		healthBar = GetNode("Body/Status/HealthContainer/HealthBar") as Control;

		var nameLable = GetNode("Body/Status/Name") as Label;

		nameLable.Text = "LV" + level + " " + System.IO.Path.GetFileNameWithoutExtension(stats.ResourcePath);

		Vector2 size = new Vector2();
		//Hacky way to make Godot recalculate object sizes
		nameLable.Hide();
		nameLable.Show();
		status.Hide();
		status.Show();
		size.x = Math.Max(texture.GetWidth(), status.RectSize.x * status.RectScale.x + 5); //add 5 for padding
		size.y = status.RectSize.y + texture.GetHeight();
		Size = size;

		var newPositionX = (status.RectSize.x * status.RectScale.x - texture.GetWidth()) / 2;
		monsterSprite.Position = new Vector2(newPositionX, monsterSprite.Position.y);
	}
	
	public override void Hurt(int currentHealth)
	{
		base.Hurt(currentHealth);
		healthBar.RectScale = new Vector2((float)currentHealth/TotalHealth, healthBar.RectScale.y);
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }

	public static Monster CreateMonster(String name, int totalHealth, int level, Node parent)
	{
		monsterScene = GD.Load<PackedScene>("res://Scenes/Monster.tscn");
		MonsterStats stats = GD.Load<MonsterStats>("res://Monsters/" + name + ".tres");
		Monster monster = monsterScene.Instance() as Monster;
		parent.AddChild(monster);
		monster.SetStats(stats, totalHealth, level);

		return monster;
	}

}
