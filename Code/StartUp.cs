using Godot;
using System;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

public class StartUp : Node2D
{
	private static readonly int overlayPort = 9002;
	private static readonly string Server = "192.168.0.14";
	//private static readonly string Server = "184.3.197.189";
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	private MonsterSpawner monsterSpawner = null;
	private MonsterSpawner itemSpawner = null;
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		GetTree().Root.TransparentBg = true;
		monsterSpawner = GetNode("MonsterSpawner") as MonsterSpawner;
		itemSpawner = GetNode("ItemSpawner") as MonsterSpawner;
		//itemSpawner.Add(Item.CreateItem("Thunder", this));
		//itemSpawner.Add(Item.CreateItem("Blizzard", this));
		//monsterSpawner.Add(Monster.CreateMonster("Dummy", 10, 4, this));
		//monsterSpawner.Add(Monster.CreateMonster("Dummy", 10, 4, this));
		/*monsterSpawner.Add(Monster.CreateMonster("Dummy", this));
		monsterSpawner.Add(Monster.CreateMonster("Dummy", this));
		monsterSpawner.Add(Monster.CreateMonster("Dummy", this));

		itemSpawner = GetNode("ItemSpawner") as MonsterSpawner;
		itemSpawner.Add(Item.CreateItem("JagSword", this));
		itemSpawner.Add(Item.CreateItem("JagSword", this));
		itemSpawner.Add(Item.CreateItem("JagSword", this));
		itemSpawner.Add(Item.CreateItem("JagSword", this));
		itemSpawner.Add(Item.CreateItem("JagSword", this));*/

		var thread = new System.Threading.Thread(WatchForCommands);
		thread.Start();
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		if(Input.IsActionJustReleased("ui_accept"))
		{
			monsterSpawner.Remove(0);
		}
	}

	public void WatchForCommands()
	{
		try
		{	
			TcpClient client = new TcpClient(Server, overlayPort);
			var reader = new StreamReader(client.GetStream());

			while(true)
			{
				string cmdStr = reader.ReadLine();
				string[] cmd = cmdStr.Split(' ');
				
				if(cmd.Length > 0)
				{
					if(cmd[0] == "addmonster")
					{
						// addmonster monsterName totalHealth level
						monsterSpawner.Add(Monster.CreateMonster(cmd[1], int.Parse(cmd[2]), int.Parse(cmd[3]), this));
					}
					else if(cmd[0] == "monsterkilled")
					{
						if(int.TryParse(cmd[1], out int monsterIndex))
						{
							monsterSpawner.Remove(monsterIndex);
						}
					}
					else if(cmd[0] == "additem")
					{
						itemSpawner.Add(Item.CreateItem(cmd[1], this));
					}
					else if(cmd[0] == "removeitem")
					{
						if(int.TryParse(cmd[1], out int itemIndex))
						{
							itemSpawner.Remove(itemIndex);
						}
					}
					else if(cmd[0] == "monsterhit")
					{
						//monsterhit monsterIndex currentHealth
						if(int.TryParse(cmd[1], out int monsterIndex))
						{
							monsterSpawner.HurtAnimation(monsterIndex, int.Parse(cmd[2]));
						}
					}
					else if(cmd[0] == "attackfellowship")
					{
						if(int.TryParse(cmd[1], out int monsterIndex))
						{
							monsterSpawner.AttackAnimation(monsterIndex);
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			GD.PrintErr(e.ToString());
		}
	}
}
